<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group(['prefix' => 'student' , 'middleware' => 'auth:api'], function () {
    Route::get('courses', 'StudentController@GetAllCourses');
    Route::get('mycourses', 'StudentController@GetAllMyCourses');
    Route::post('enroll', 'StudentController@Enroll');
    Route::post('unenroll', 'StudentController@UnEnroll');
    Route::get('statistics', 'StudentController@getMySession');
});

Route::group(['prefix' => 'teacher' , 'middleware' => 'auth:api'], function () {
    Route::get('courses', 'TeacherController@GetMycourses');
    Route::get('statistics', 'TeacherController@GetCourseStats');
    Route::post('addCourses', 'TeacherController@AddCourse');
    Route::post('TakeAttendance', 'TeacherController@TakeAttendance');
});