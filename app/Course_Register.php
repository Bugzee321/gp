<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_Register extends Model
{
    protected $fillable = ['user_id' , 'crs_id'];
}
