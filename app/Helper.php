<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Helper extends Model
{

    /** Take the code, body, message from the function and return them as json a format */
    public static function api_response_format($code, $body = [], $message = [])
    {
        return [
            'code' => $code,
            'body' => $body,
            'message' => $message
        ];
    }

    public static function uploadImage($imageData)
    {
        $fileName = uniqid() . '.png';
        $imageData = str_replace(' ' , '+' ,$imageData);
        $img = Image::make($imageData);
        $img->resize(640 , 480);
        $img->save(public_path('users/' . $fileName));
        return public_path('users/' . $fileName);
    }
}
