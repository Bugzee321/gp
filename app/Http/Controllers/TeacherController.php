<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_Register;
use App\Helper;
use App\session;
use App\session_attendance;
use Illuminate\Http\Request;
use App\User;
class TeacherController extends Controller
{
    public function GetMycourses(Request $request){
        $courses = Course::whereTeacher_id($request->user()->id)->get();
        foreach ($courses as $course){
            $course->teachername = User::find($course->teacher_id)->name;
        }
        return Helper::api_response_format(200 , $courses , '');
    }

    public function AddCourse(Request $request){
        $course = new Course([
            'name' => $request->name,
            'description' => $request->description,
            'access_code' => $request->access_code,
            'teacher_id' => $request->user()->id,
        ]);
        $course->save();
        return Helper::api_response_format(200 , $course , 'Course Created Successfully');
    }

    public function GetCourseStats(Request $request){
        $result = [];
        $sessions = session::whereCrs_id($request->course)->get();
        $all = Course_Register::whereCrs_id($request->course)->count();
        foreach ($sessions as $session){
            $result[$session->name]['attend'] = session_attendance::whereSession_id($session->id)->count();
            $result[$session->name]['absent'] = $all - session_attendance::whereSession_id($session->id)->count();
            $result[$session->name]['users'] = session::getAttended($session->id);
        }
        return Helper::api_response_format(200 , $result , '');
    }

    public function TakeAttendance(Request $request){
        $session = New session([
            'name' => $request->name,
            'description' => $request->description,
            'crs_id' => $request->course
        ]);
        $session->save();
        $real = [];
        $image = Helper::uploadImage($request->image);
        $path = public_path('users/model/recognize.py');
        $ids = Course_Register::whereCrs_id($request->course)->get(['user_id'])->toArray();
        foreach ($ids as $id){
            $real[] = $id['user_id'];
        }
	    $real = json_encode($real);
        exec("python3 $path $image $real 2>&1" ,$x);
        $attended = eval('return ' . $x[0] . ';');
        foreach ($attended as $person){
            $attend = New session_attendance([
                'user_id' => $person,
                'session_id' => $session->id
            ]);
            $attend->save();
        }
        return Helper::api_response_format(200 , [] , 'Attendance Taken Successfully');
    }
}

