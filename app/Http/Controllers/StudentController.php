<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_Register;
use App\Helper;
use App\session;
use App\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function GetAllMyCourses(Request $request){
        $ids = Course_Register::whereUser_id($request->user()->id)->get(['crs_id']);
        $courses = Course::whereIn('id' , $ids)->get();
        foreach ($courses as $course){
            $course->teachername = User::find($course->teacher_id)->name;
        }
        return Helper::api_response_format('200' , $courses , '');
    }

    public function GetAllCourses(Request $request){
        $courses = Course::get();
        foreach ($courses as $course){
            $course->teachername = User::find($course->teacher_id)->name;
        }
        return Helper::api_response_format('200' , $courses , '');
    }

    public function Enroll(Request $request){
        $enroll = Course_Register::create([
            'user_id' => $request->user()->id,
            'crs_id' => $request->course
        ]);
        return Helper::api_response_format('200' , Course::find($enroll->crs_id) , 'You Have Enrolled Successfully');
    }

   public function UnEnroll(Request $request){
        Course_Register::whereUser_id($request->user()->id)->whereCrs_id($request->course)->delete();
        return Helper::api_response_format(200 , '' , 'User nomore enrolled in this course');
   }

   public function getMySession(Request $request){
        $sessions = session::whereCrs_id($request->course)->get();
        foreach ($sessions as $session){
            $session->status = session::getStatus($session->id , $request->user()->id);
        }
        return Helper::api_response_format(200 , $sessions , 'Hello Mother Fuckers');
   }
}