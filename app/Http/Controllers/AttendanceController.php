<?php

namespace App\Http\Controllers;

use App\Helper;
use App\session_attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{
    public function TakeAttendance(Request $request){
        $validator = Validator::make($request->all() , [
            'user_id' => '',
            'session_id' => ''
        ]);
        if ($validator->fails())
            return Helper::api_response_format(400 , $validator->errors() , 'Something went wrong');

        session_attendance::create([
            'user_id' => $request->user_id,
            'session_id' => $request->session_id
        ]);
        return Helper::api_response_format(200 , 'attended Successfully');
    }
}