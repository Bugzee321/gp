<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class session extends Model
{
    protected $fillable = ['name' , 'description' , 'crs_id'];
    public static function getStatus($session_id , $user_id){
        if(count(session_attendance::whereUser_id($user_id)->whereSession_id($session_id)->get()) > 0)
            return 'Attend';
        return 'Absent';
    }

    public static function getAttended($session_id){
        $ids = session_attendance::whereSession_id($session_id)->get(['user_id']);
        return User::whereIn('id' , $ids)->get();
    }
}
