<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class session_attendance extends Model
{
    protected $fillable = ['user_id' , 'session_id'];
}
